package com.fuzzingtheweb.hnreader.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.fuzzingtheweb.hnreader.R;
import com.fuzzingtheweb.hnreader.fragments.PostListFragment;


public class MainActivity extends BaseActivity implements PostListFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_main);

        if (isNetworkAvailable() == false) {
            Toast.makeText(this, "Network is unavailable", Toast.LENGTH_LONG).show();
            return;
        }
    }

    /**
     * Callback method from {@link PostListFragment}
     * indicating that the post with the given url was selected.
     */
    @Override
    public void onItemClick(String postUrl) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.setData(Uri.parse(postUrl));
        startActivity(intent);
    }
}
